# manifest

* charts/service-a  

1. 通过value.yaml进行配置，包含配套的hpa、service、ingress、deployment、configmap等。  
2. 抽离配置项至configmap，可通过values.yaml更改配置，helm升级时会对比配置，有更新则触发pod的重启。


* Dockerfile  

1. 采用多阶段构建模式，构建镜像和应用镜像分离，保证了应用镜像的干净以及体积。

* main.tf  

1. Terraform主要代码

* fix a service-a/README.md typo
