# Service A

A fake service named `service A` which is used for hands-on test

# Build
```
export GOPROXY=https://goproxy.cn
git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git
cd service-a
go mod tidy
go build 
```

# Config
Edit `config.yaml`
```
env: test  # Set environment name
port: 8888 # Set port to which the service listens to, defaults to `8080`
feature_one: true # Set to `true` if `feature_one` is enabled 
feature_two: false # Set to `true` if `feature_tow` is enabled 
```

# Usage
```
$ ./service-a -h
NAME:
   service-a - Service A

USAGE:
   service-a [global options] command [command options] [arguments...]

VERSION:
   v0.0.1

AUTHOR:
   Shiyuan Chen <sychen@gitlab.cn>

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --file FILE, -f FILE  Load configuration from FILE (default: "config.yaml")
   --help, -h            show help (default: false)
   --version, -v         print the version (default: false)

COPYRIGHT:
   (c) 2021 JiHu GitLab
```
# Run
```
./service-a --file config.yaml
```
## APIs
### healtch check  - `GET /healthz`
example response
```
{
  "message": "Service a is running well",
  "status": "OK"
}
```
### list features - `GET /api/v1/features`
example response
```
{
  "Env": "test",
  "Port": "8888",
  "FeatureOne": true,
  "FeatureTwo": false
}
```
